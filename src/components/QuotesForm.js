import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {CATEGORIES} from "../constans";

class QuotesForm extends Component {
  constructor(props) {
    super(props);

    if (props.quote) {
      this.state = {...props.quote};
    } else {
      this.state = {
        name: '',
        category: Object.keys(CATEGORIES)[0],
        text: ''
      };
    }

  }


  valueChanged = event => {
    const {name, value} = event.target;
    this.setState({[name]: value});
  };

  submitHandler = event => {
    event.preventDefault();
    this.props.onSubmit({...this.state});
  };

  render() {
    return (
      <Form className="QuotesForm" onSubmit={this.submitHandler}>
        <FormGroup row>
          <Label for="category" sm={2}>Email</Label>
          <Col sm={10}>
            <Input type="select" name="category" id="category"
                   value={this.state.category} onChange={this.valueChanged}
            >
              {Object.keys(CATEGORIES).map(categoryId => (
                <option key={categoryId} value={categoryId}>{CATEGORIES[categoryId]}</option>
              ))}
            </Input>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label for="name" sm={2}>Author</Label>
          <Col sm={10}>
            <Input type="text" name="name" id="name" placeholder="Author"
                   value={this.state.name} onChange={this.valueChanged}
            />
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label for="text" sm={2}>Quote text</Label>
          <Col sm={10}>
            <Input type="textarea" name="text" placeholder="Quote text"
                   value={this.state.text} onChange={this.valueChanged}
            />
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col sm={{size: 10, offset: 2}}>
            <Button type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default QuotesForm;