import React, {Component, Fragment} from 'react';
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {Route, Switch, NavLink as RouterNavLink} from "react-router-dom";

import AddQuotes from "./containers/AddQuotes";

import './App.css';
import QuotesList from "./containers/QuotesList";
import EditQuotes from "./containers/EditQuotes";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Navbar color="dark" dark expand="md">
          <NavbarBrand>Quotes Central</NavbarBrand>
          <NavbarToggler />
          <Collapse isOpen navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink tag={RouterNavLink} to="/" exact >Quotes</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={RouterNavLink} to="/add">Submit new quote</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>

        <Container>
          <Switch>
            <Route path="/" exact component={QuotesList} />
            <Route path="/add" exact component={AddQuotes} />
            <Route path="/quotes/:id/edit" component={EditQuotes} />
            <Route path="/quotes/:categoryId" component={QuotesList}  />
            <Route render={() => <h1>Not found</h1>} />
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default App;
