export const CATEGORIES = {
  'star-wars': 'Star Wars',
  'famous-people': 'Famous people',
  'saying': 'Saying',
  'humour': 'Humour',
  'motivational': 'Motivational'
};