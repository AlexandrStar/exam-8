import React, {Component} from 'react';
import {Button, Card, CardText, CardTitle, Col, Nav, NavItem, NavLink, Row} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import {CATEGORIES} from "../constans";
import axios from '../axios-quotes';

class QuotesList extends Component {
  state = {
    quotes: null
  };

  loadData() {
    let url = 'quotes.json';
    const categoryId = this.props.match.params.categoryId;

    if (categoryId){
      url += `?orderBy="category"&equalTo="${categoryId}"`;
    }
    axios.get(url).then(response => {
      if (!response.data) return;
      const quotes = Object.keys(response.data).map(id => {
        return {...response.data[id], id}
      });
      this.setState({quotes});
    })
  }

  handleRemove = (id) => {
    axios.delete('quotes/' + id + '.json')
      .then(() => {
        this.loadData();
      })
  };

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
      this.loadData();
    }
  }



  render() {
    let quotes = null;

    if (this.state.quotes) {
      quotes = this.state.quotes.map(quote => (
        <Card key={quote.id} style={{marginBottom: "15px"}} body>
          <CardText>{quote.text}</CardText>
          <CardTitle>--{quote.name}</CardTitle>
          <Col sm={{size: 10}}>
            <RouterNavLink to={'/quotes/' + quote.id + '/edit'}>
              <Button style={{marginRight: "10px"}} color="info">Edit</Button>
            </RouterNavLink>
              <Button onClick={() => this.handleRemove(quote.id)} style={{marginRight: "10px"}} color="danger">Delete</Button>
          </Col>
        </Card>
      ));
    }

    return (
      <Row style={{marginTop: "20px"}}>
        <Col sm={3}>
          <Nav vertical>
            <NavItem>
              <NavLink tag={RouterNavLink} to="/" exact>All</NavLink>
            </NavItem>
            {Object.keys(CATEGORIES).map(categoryId => (
              <NavItem key={categoryId}>
                <NavLink
                  tag={RouterNavLink}
                  to={"/quotes/" + categoryId}
                  exact
                >
                  {CATEGORIES[categoryId]}
                </NavLink>
              </NavItem>
            ))}
          </Nav>
        </Col>
        <Col sm={9}>
            {quotes}
        </Col>
      </Row>
    );
  }
}

export default QuotesList;