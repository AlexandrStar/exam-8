import React, {Component, Fragment} from 'react';
import axios from '../axios-quotes';
import QuotesForm from "../components/QuotesForm";

class AddQuotes extends Component {
  addQuote = quote => {
    axios.post('quotes.json', quote).then(() => {
      this.props.history.replace('/');
    })
  };

  render() {
    return (
      <Fragment>
        <h1>Submit new quote</h1>
        <QuotesForm onSubmit={this.addQuote}/>
      </Fragment>
    );
  }

}

export default AddQuotes;